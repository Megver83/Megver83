# Hi there, I'm [David](https://www.pizarro.info/), better known as Megver83 👋

<!-- [![Email Badge](https://img.shields.io/badge/Email-contacto%40daveepark.anonaddy.com-blue?style=flat-square)](mailto:contacto@daveepark.anonaddy.com) -->
[![Telegram Badge](https://img.shields.io/badge/Telegram-@daveepark-blue?style=flat-square&logo=telegram)](https://t.me/daveepark)
[![LinkedIn Badge](https://img.shields.io/badge/LinkedIn-David%20Pizarro%20Naranjo-blue?style=flat-square&logo=linkedin)](https://www.linkedin.com/in/david-pizarro-naranjo-4a1848316/)
[![XMPP Badge](https://img.shields.io/badge/XMPP-megver83%40jabjab.de-blue?style=flat-square&logo=xmpp)](xmpp:megver83@jabjab.de)
[![Mastodon Badge](https://img.shields.io/badge/Mastodon-@megver-blue?style=flat-square&logo=mastodon)](https://mastodon.cl/@megver)
[![Discord Badge](https://img.shields.io/badge/Discord-Megver83-blue?style=flat-square&logo=discord)](https://discord.com/users/817488629341356083)
[![GitLab Badge](https://img.shields.io/badge/GitLab-Megver83-blue?style=flat-square&logo=gitlab)](https://gitlab.com/Megver83)
[![GitHub Badge](https://img.shields.io/badge/GitHub-Megver83-blue?style=flat-square&logo=github)](https://github.com/Megver83)

<details>
  <summary>Non-humans also know me...</summary>
  <p align="center">
    <img src="https://gitlab.com/Megver83/Megver83/-/raw/main/Megver83_ChatGPT.gif" />
  </p>
</details>

---

I'm a student of Computer Engineering at [Universidad Católica del Norte](https://www.ucn.cl/) in Chile, passionate about Pythonic programming, backend development, and contributing to Free/Libre Open Source Software (FLOSS).

### Technologies and Tools

[![Git Badge](https://img.shields.io/badge/-Git-F05032?style=flat-square&logo=git&logoColor=white)](https://git-scm.com/)
[![GitHub Badge](https://img.shields.io/badge/-GitHub-181717?style=flat-square&logo=github)](https://github.com/)
[![GitLab Badge](https://img.shields.io/badge/-GitLab-FCA121?style=flat-square&logo=gitlab&logoColor=white)](https://gitlab.com/)
[![Linux Badge](https://img.shields.io/badge/-Linux-FCC624?style=flat-square&logo=linux&logoColor=black)](https://www.linux.org/)
[![Nextcloud Badge](https://img.shields.io/badge/-Nextcloud-0082C9?style=flat-square&logo=nextcloud&logoColor=white)](https://nextcloud.com/)
[![Apache2 Badge](https://img.shields.io/badge/-Apache2-D22128?style=flat-square&logo=apache&logoColor=white)](https://httpd.apache.org/)
[![PostgreSQL Badge](https://img.shields.io/badge/-PostgreSQL-336791?style=flat-square&logo=postgresql&logoColor=white)](https://www.postgresql.org/)
[![NestJS Badge](https://img.shields.io/badge/-NestJS-E0234E?style=flat-square&logo=nestjs&logoColor=white)](https://nestjs.com/)
[![Python Badge](https://img.shields.io/badge/-Python-3776AB?style=flat-square&logo=python&logoColor=white)](https://www.python.org/)
[![Flask Badge](https://img.shields.io/badge/-Flask-000000?style=flat-square&logo=flask&logoColor=white)](https://flask.palletsprojects.com/)
[![Arch Linux Badge](https://img.shields.io/badge/-Arch%20Linux-1793D1?style=flat-square&logo=arch-linux&logoColor=white)](https://www.archlinux.org/)
[![JavaScript Badge](https://img.shields.io/badge/-JavaScript-F7DF1E?style=flat-square&logo=javascript&logoColor=black)](https://developer.mozilla.org/en-US/docs/Web/JavaScript)
[![TypeScript Badge](https://img.shields.io/badge/-TypeScript-3178C6?style=flat-square&logo=typescript&logoColor=white)](https://www.typescriptlang.org/)
[![Google Colab Badge](https://img.shields.io/badge/-Google%20Colab-F9AB00?style=flat-square&logo=google-colab&logoColor=white)](https://colab.research.google.com/)
[![Jupyter Badge](https://img.shields.io/badge/-Jupyter-F37626?style=flat-square&logo=jupyter&logoColor=white)](https://jupyter.org/)
[![Raspberry Pi Badge](https://img.shields.io/badge/-Raspberry%20Pi-C51A4A?style=flat-square&logo=raspberry-pi&logoColor=white)](https://www.raspberrypi.org/)
[![Debian Badge](https://img.shields.io/badge/-Debian-A81D33?style=flat-square&logo=debian&logoColor=white)](https://www.debian.org/)
[![Docker Badge](https://img.shields.io/badge/-Docker-2496ED?style=flat-square&logo=docker&logoColor=white)](https://www.docker.com/)
[![Kubernetes Badge](https://img.shields.io/badge/-Kubernetes-326CE5?style=flat-square&logo=kubernetes&logoColor=white)](https://kubernetes.io/)
[![Bash Badge](https://img.shields.io/badge/-Bash-4EAA25?style=flat-square&logo=gnu-bash&logoColor=white)](https://www.gnu.org/software/bash/)
[![QEMU](https://img.shields.io/badge/QEMU-FF6600?style=flat-square&logo=qemu&logoColor=white)](https://www.qemu.org/)
[![WordPress](https://img.shields.io/badge/WordPress-21759B?style=flat-square&logo=wordpress&logoColor=white)](https://wordpress.org/)
[![Elementor](https://img.shields.io/badge/Elementor-92003B?style=flat-square&logo=elementor&logoColor=white)](https://elementor.com/)
[![GnuPG](https://img.shields.io/badge/GnuPG-0093DD?style=flat-square&logo=gnuprivacyguard&logoColor=white)](https://gnupg.org/)
[![LaTeX](https://img.shields.io/badge/LaTeX-008080?style=flat-square&logo=latex&logoColor=white)](https://www.latex-project.org/)
[![Thunderbird](https://img.shields.io/badge/Thunderbird-0A84FF?style=flat-square&logo=thunderbird&logoColor=white)](https://www.thunderbird.net/)
[![VSCodium](https://img.shields.io/badge/VSCodium-2F80ED?style=flat-square&logo=vscodium&logoColor=white)](https://vscodium.com/)
[![Firefox](https://img.shields.io/badge/Firefox-FF7139?style=flat-square&logo=firefox-browser&logoColor=white)](https://www.mozilla.org/firefox/)
[![Markdown](https://img.shields.io/badge/Markdown-000000?style=flat-square&logo=markdown&logoColor=white)](https://www.markdownguide.org/)
[![LineageOS](https://img.shields.io/badge/LineageOS-167C80?style=flat-square&logo=lineageos&logoColor=white)](https://www.lineageos.org/)
[![Android](https://img.shields.io/badge/Android-3DDC84?style=flat-square&logo=android&logoColor=white)](https://www.android.com/)
[![KDE](https://img.shields.io/badge/KDE-1D99F3?style=flat-square&logo=kde&logoColor=white)](https://kde.org/)
[![Kdenlive](https://img.shields.io/badge/Kdenlive-4EB1BA?style=flat-square&logo=kdenlive&logoColor=white)](https://kdenlive.org/)
[![Hugo](https://img.shields.io/badge/Hugo-FF4088?style=flat-square&logo=hugo&logoColor=white)](https://gohugo.io/)
[![Apollo GraphQL](https://img.shields.io/badge/Apollo%20GraphQL-311C87?style=flat-square&logo=apollographql&logoColor=white)](https://www.apollographql.com/)
[![Ardour](https://img.shields.io/badge/Ardour-C7302B?style=flat-square&logo=ardour&logoColor=white)](https://ardour.org/)
[![Caddy](https://img.shields.io/badge/Caddy-F5C400?style=flat-square&logo=caddy&logoColor=black)](https://caddyserver.com/)
[![Dovecot](https://img.shields.io/badge/Dovecot-0078D4?style=flat-square&logo=dovecot&logoColor=white)](https://www.dovecot.org/)
[![MySQL](https://img.shields.io/badge/MySQL-4479A1?style=flat-square&logo=mysql&logoColor=white)](https://www.mysql.com/)
[![SQLite](https://img.shields.io/badge/SQLite-003B57?style=flat-square&logo=sqlite&logoColor=white)](https://www.sqlite.org/)
[![Zsh](https://img.shields.io/badge/Zsh-1A2C34?style=flat-square&logo=zsh&logoColor=white)](https://www.zsh.org/)
[![cURL](https://img.shields.io/badge/cURL-20232A?style=flat-square&logo=curl&logoColor=white)](https://curl.se/)

### About Me

- 🎓 Currently in my fourth year of university
- 💻 Passionate about backend development and DevOps
- 🥋 3rd kyu judoka, that is green belt
- 🌐 Native Spanish speaker, fluent in English since age 10
- 🐧 Advocate for and user of [FSDG-compliant GNU/Linux distributions](https://www.gnu.org/distros/free-distros.html)
- 🖥️ [Developer at Parabola GNU/Linux-libre](https://www.parabola.nu/people/hackers/#megver83) since 2017

---

![Megver83's GitHub Stats](https://github-readme-stats.vercel.app/api?username=Megver83&show_icons=true)

[![License](https://img.shields.io/github/license/Megver83/Megver83?style=flat-square)](https://github.com/Megver83/Megver83/blob/main/LICENSE)
[![Last Commit](https://img.shields.io/github/last-commit/Megver83/Megver83?style=flat-square)](https://github.com/Megver83/Megver83/commits/main)
[![Visits](https://komarev.com/ghpvc/?username=Megver83&style=flat-square&color=blueviolet)](https://github.com/Megver83)
